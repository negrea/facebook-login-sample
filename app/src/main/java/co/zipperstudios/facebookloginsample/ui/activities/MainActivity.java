package co.zipperstudios.facebookloginsample.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import co.zipperstudios.facebookloginsample.R;
import co.zipperstudios.facebookloginsample.ui.fragments.LoggedInFragment;
import co.zipperstudios.facebookloginsample.ui.fragments.LoggedOutFragment;

public class MainActivity extends FragmentActivity implements LoggedOutFragment.OnFragmentInteractionListener, LoggedInFragment.OnFragmentInteractionListener {

    private static final String LOGGED_IN_FRAGMENT_TAG = "logged_in_fragment_tag";
    private static final String LOGGED_OUT_FRAGMENT_TAG = "logged_out_fragment_tag";

    CallbackManager mCallbackManager;

    Handler mHandler = new Handler();

    AccessTokenTracker mAccessTokenTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {
                updateWithToken(newAccessToken);
            }
        };
        mAccessTokenTracker.startTracking();

        // Handler face codul rulat inauntrul metodei "run" sa fie executat pe UI thread.
        // Facem asta ca sa returneze AccessToken.getCurrentAccessToken() diferit de null
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

                if (isUserLoggedIn()) {
                    Log.d("Facebook", "Logged in");
                    fragmentTransaction.add(R.id.fragment_container, LoggedInFragment.newInstance("Parameter 1", "Parameter 2"), LOGGED_IN_FRAGMENT_TAG);
                } else {
                    Log.d("Facebook", "NOT Logged in");
                    fragmentTransaction.add(R.id.fragment_container, LoggedOutFragment.newInstance("Parameter 1", "Parameter 2"), LOGGED_OUT_FRAGMENT_TAG);
                }

                fragmentTransaction.commit();
            }
        }, 0);

        // Setting callbacks for the facebook button
        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Log.d("Facebook Login", "success");
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d("Facebook Login", "cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("Facebook Login", "error");
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        // This is used for communication between the fragment and activity.
        // We do not use it here
    }

    /**
     * Method used for listening for access token changes. For showing the logged out fragment.
     * @param currentAccessToken
     */
    private void updateWithToken(AccessToken currentAccessToken) {
        // Fragment Transaction is used for 
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (isUserLoggedIn(currentAccessToken)) {
            fragmentTransaction.add(R.id.fragment_container, LoggedInFragment.newInstance("Parameter 1", "Parameter 2"), LOGGED_IN_FRAGMENT_TAG);
        } else {
            fragmentTransaction.add(R.id.fragment_container, LoggedOutFragment.newInstance("Parameter 1", "Parameter 2"), LOGGED_OUT_FRAGMENT_TAG);
        }

        fragmentTransaction.commit();
    }

    private boolean isUserLoggedIn() {
        return isUserLoggedIn(AccessToken.getCurrentAccessToken());
    }

    private boolean isUserLoggedIn(AccessToken accessToken) {
        return accessToken != null;
    }
}
