package co.zipperstudios.facebookloginsample.utilities;

import android.app.Application;

import com.facebook.FacebookSdk;

/**
 * Created by Vlad on 25/07/16.
 */

public class FacebookLoginSampleApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
